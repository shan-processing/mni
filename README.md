# MNI

MNI registration modified from Blake Dewey's [pipeline](https://gitlab.com/iacl/iacl_pipeline).

This package registers longitudinal MR images to the [ICBM 2009c nonlinear symmetric template](http://www.bic.mni.mcgill.ca/ServicesAtlases/ICBM152NLin2009) using commands from [ANTs](http://stnava.github.io/ANTs) suite. A tool (`avscale`) from [FSL](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/) is also used to extract rigid transformation.

The first input image to the command `perform_mni.sh` is rigidly registered to the MNI template and other images are rigidly registered to the first input image using `antsRegistration `. `antsAffineInitializer` is used to find the initial alignment to increase the registration accuracy.

When image mask is specified, the registration metric is only eveluated within the mask, therefore increasing the registration accuracy when the brain is cut-off partially.

### Installation

1. Install [ANTs](http://stnava.github.io/ANTs)
2. Install [FSL](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/)
3. Make sure `antsRegistration`, `antsApplyTransforms`, `ResampleImageBySpacing`, `ConvertTransformFile`, `antsAffineInitializer`, and `avscale` are in your `$PATH`
4. `pip install git+<repo-url>`

### Usage

Register image `image1.nii.gz` and `image2.nii.gz`, and save the results into `output_directory1` and `output_directory2`, respectively:
```bash
perform_mni.sh -i image1.nii.gz -i image2.nii.gz -o output_directory1 -o output_directory2
```

Register with masks:
```bash
perform_mni.sh -i image1.nii.gz -i image2.nii.gz -o output_directory1 -o output_directory2 -m mask1.nii.gz -m mask2.nii.gz
```

Transform the image masks into MNI space as well:
```bash
perform_mni.sh -i image1.nii.gz -i image2.nii.gz -o output_directory1 -o output_directory2 -m mask1.nii.gz -m mask2.nii.gz -t
```

Check `perform_mni.sh -h` for more details.
